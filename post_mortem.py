#!/usr/bin/env python3

from src import poll, util

from collections import deque

matchup_queue = deque(filter(lambda x:len(x)>0, map(str.strip, open('participants').readlines())))
winners = filter(lambda x:len(x)>0, map(str.strip, open('actual.bracket').readlines()))

if len(matchup_queue) % 2 == 1:
    gatekeeper = matchup_queue.pop()
else:
    gatekeeper = None

right_sum = 0
wrong_sum = 0
dir_right = 0
dir_wrong = 0
while len(matchup_queue) > 1 or gatekeeper:
    a = matchup_queue.popleft()
    if len(matchup_queue) > 0:
        b = matchup_queue.popleft()
    else:
        b = gatekeeper
        gatekeeper = None
    w = next(winners)
    k = util.escape_name(a) + '.' + util.escape_name(b)
    p = poll.Poll(k)
    pred = ( p.median(), p.mean() )
    if a == w:
        prob = max(pred)
        right_sum += prob
        wrong_sum += 1 - prob
        if prob > 0.5:
            dir_right += 1
        elif prob < 0.5:
            dir_wrong += 1
    elif b == w:
        prob = min(pred)
        right_sum += 1 - prob
        wrong_sum += prob
        if prob < 0.5:
            dir_right += 1
        elif prob > 0.5:
            dir_wrong += 1
    else:
        print(f"In a matchup between {a} vs. {b}, you claim {w} won. Fix this.")
        exit(1)
    print(f"a={a},b={b},w={w},prob={100.0*prob:.4g}%")
    matchup_queue.append(w)

print( "\nVoters were {:.3g}% correct.".format(100.0 * right_sum / (right_sum + wrong_sum)) )
print( "We (implied) 'called' {} directionally correct, {} wrong, aka we got {:.3g}% directionally correct.".format(
    dir_right,
    dir_wrong,
    (100.0 * dir_right / (dir_right + dir_wrong))
     ))

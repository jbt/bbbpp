#!/usr/bin/env python3

from itertools import product
from sys import argv, exit
import json

args = argv[1:]
if '--verbose' in args:
    verbose = True
    args.remove('--verbose')
else:
    verbose = False
if '--hyper-links' in args:
    linked_output = True
    args.remove('--hyper-links')
else:
    linked_output = False

from src import compare, wiki

def describe_match(tup):
    h = tup[1]
    cond = h['cond']
    if cond and cond != 'Unspecified':
        cond = ' by '+cond
    else:
        cond = ''
    vs = h['vs']
    season = h['season']
    return f"beat {vs}{cond} in \"{season}\""

def disp(ans):
    print(compare.path_desc_short(ans,linked_output))
    if verbose:
        print(compare.path_desc_long(ans,linked_output))

def resolve( a, b ):
    if not a:
        if b:
            return resolve( b, a )
        else:
            print( 'Inconclusive.')
            return None
    if b:
        if len(a) > len(b):
            return resolve( b, a )
        disp(a)
        disp(b)
        if len(a) == len(b):
            print('Inconclusive.')
            return None
    else:
        disp(a)
    print(a[0][0],'wins.')
    return a
    

if len(args) == 2:
    a = args[0]
    b = args[1]
    if a == b:
        exit(0)
    ( l, r ) = compare.do_comparison( a, b )
    resolve( l, r )
else:
    for b in args:
        rec = wiki.winloss_record(b)
        for m in rec:
            print(b,':',json.dumps(m))
        wl = wiki.lifetime_winloss(b)
        print(b,'(',wl[0],',',wl[1],')')


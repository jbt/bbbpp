#!/usr/bin/env python3

import json
import math
import sys
from src import bracket, compare, misskey, opts, poll, util

verbose = opts.args.verbose 

unpolled_prob = 0.5 if opts.args.unpolled_is_even > 0 else 0.01

if verbose:
    print(opts.args)

poll.freshen()

bracket_tree = bracket.load(unpolled_prob)

winners = list(map(lambda e: [ e[1], e[0] ], bracket_tree[-1][0].possibles() ) )
winners.sort(reverse = True)

total = sum(map(lambda x: x[0], winners))
print('\nPrediction {:.3g}% complete. Win probabilities:'.format(100*total) )
for winner in winners:
    normalized = winner[0] / total
    print( '\t{:.3g}%'.format(normalized * 100.0), winner[1] )

to_poll = bracket.unpolled_matches()
if len(to_poll) == 0:
    print('Polling all done.')
    exit(0)
print(f"\nCreate a poll about one of the {len(to_poll)} yet-unpolled matches:\n")
width = max(map(lambda x: max(len(x[1]['b']),len(x[1]['a'])), to_poll[0:9])) + 1
divider = '=' * ( width * 9 )
print( divider )
for (i,info) in enumerate(to_poll[0:9]):
    p = info[0]
    info = info[1]
    rnd = info['Round']
    if type(rnd) == int:
        rnd = 'Round ' + str(rnd)
    comps = compare.do_comparison(info['a'], info['b'], 3e2)
    comps = map(compare.path_desc_short, comps)
    fmt = '{} {:.3f}: {:9s}{:' + str(width) + 's} vs. {:' + str(width) + 's}'
    print(fmt.format(i + 1, p, rnd, info['a'], info['b']))
    print('\t', next(comps))
    print('\t', next(comps))
    #print(divider)
print(divider)

answer = input("Entry you'd like to poll (or 0/default for none of them).")
if answer.isnumeric() and int(answer) > 0:
    i = int(answer) - 1
    p = to_poll[i]
    comment = input('Comment on this match:')
    if verbose:
        print('Send request on ', p)
    poll.create( p[1], comment )

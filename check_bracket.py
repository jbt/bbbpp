#!/usr/bin/env python3

from src import bracket, opts, poll

from glob import glob

verbose = opts.args.verbose

poll.freshen()

for bracket_file in glob('*.bracket'):
    result = 1.0
    name = bracket_file[0:-8]
    print(f"   ###   {name}   ###")
    predictions = filter(lambda x:len(x)>0, map(str.strip, open(bracket_file).readlines()))
    bracket_tree = bracket.load(0.5)
    for (round_index,bracket_row) in enumerate(bracket_tree):
        if verbose:
            print(f"\n   === Round {round_index}   ===   {result}   === ")
        for pos in bracket_row:
            if len(pos.who) == 1:
                continue
            try:
                pred = next(predictions)
            except Exception as e:
                print(bracket_file,' does not contain enough predictions, probably',e,'Proabability so far was',result)
                result = 0
                break
            if pred in pos.who:
                p = pos.who[pred]
                if verbose:
                    print(f"{name} called: {pred} : {p}" )
                if p < 0.0101:
                    print(f"Warning: you've called someone ({pred}) being given low probability, has this even been polled yet?")
                result *= p
                pos.who = { pred: 1.0 }
            else:
                print('Prediction', pred, 'is not one of the options in', str(pos))
                exit(1)
            pos.update()
        if not result:
            break
    print(f"\n \t ### \t Odds of {name}'s bracket success",100*result,'% \t ### \n')

A simple script to turn polls about predictions for given matches into probabilistic predictions for
a qualifier bracket & gatekeeper match in BattleBots: Champions

In order to run, start it with the CWD that contains the appropriate data files. These include:

## participants

This file lists out (one per line) the competitors in the order they would be if you were drawing the bracket (the order determines pairings).
If you have a gatekeeper / bounty, that entry comes last. The program will know it's different because you'd have an odd number of competitors, rather than a power of two (which are always positive).

## token
Contains your misskey API token

## bracket
Contains your personal predictions, if you'd like to play that game (call the whole bracket).
Each line contains the winner of the next consecutive match (go top-to-bottom order from participants, then secondarily finish out round 1 before going on to round 2, etc.).
So for example, if participants contains:
````
A
B
C
D
````
A valid bracket file might contain
````
A
C
A
````
This would indicate you're calling "A beats B, then C beats D, then A beats C. Winner is C."
This file is read by `check_bracket.py` and not `predict_andor_create_poll.py`

## BotA.BotB.json
These are actually written by the script, but should be left in-place so we don't lose our spot in the process. It's a cache of poll results, but if the file's missing the script will assume you haven't asked the question yet.


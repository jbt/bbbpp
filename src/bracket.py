from . import opts, poll, util

import math

verbose = opts.args.verbose 

unpolled_prob = 0.01
matchups = {}

def evaluate_results(a,b,p,rnd):
    global matchups
    k = util.match_key(a,b)
    r = poll.get(k)
    if type(rnd) == int:
        rnd = rnd + 1
    matchups[k] = { 'Poccur': p, 'a': a, 'b': b, 'k': k, 'Result': None, 'Round': rnd }
    if r:
        med = r.median()
        if med:
            men = r.mean()
            ave = med if abs(med - 0.5) > abs(men - 0.5) else men
            matchups[k]['Result'] = ave
            if verbose:
                print(k, ave)
            return ( p * ave, p * (1.0 - ave) )
        else:
            matchups[k]['Result'] = 0.5
    if verbose:
        print(k,'unpolled')
    return ( p * unpolled_prob, p * unpolled_prob )

class Position:
    def __init__(self,whole_bot = None):
        self.who = {}
        self.next_match = None
        self.round_num = 0
        self.y = 0
        if whole_bot:
            self.who[whole_bot] = 1.0
    
    def __str__(self):
        rv = f"In round {self.round_num}, match {self.y}: "
        for (n,p) in self.who.items():
            rv += ' {0:s}({1:.1f}%)'.format(n,p*100)
        return rv
    
    def set_prob(self, bot, p):
        if verbose:
            print('set_prob(',self,bot,p,')')
        self.who[bot] = p
    
    def add_prob(self, bot, p):
        if bot in self.who:
            self.who[bot] += p
        else:
            self.who[bot] = p

    def hook(self, m):
        self.next_match = m
        m.round_num = self.round_num
    
    def possibles(self):
        return self.who.items()
    
    def progress_bar(self):
        return sum(self.who.values())

    def update(self):
        if self.next_match:
            if type(self.round_num) == int:
                self.next_match.round_num = self.round_num
            self.next_match.update()

class Match:
    
    def __init__(self,l,r):
        self.left_in = l
        self.rite_in = r
        self.out = Position()
        self.round_num = 0
        l.hook(self)
        r.hook(self)
        if type(self.round_num) == int:
            self.out.round_num = self.round_num + 1 
        else:
            self.out.round_num = self.round_num

    def update(self):
        for x in [ self.left_in, self.rite_in ]:
            if x.progress_bar() > 1.0001:
                raise Exception('Too probable:'+str(x.possibles()))
        self.out.who.clear()
        if type(self.out.round_num) == int:
            self.out.round_num = self.round_num + 1
        for (an,ap) in self.left_in.possibles():
            for (bn,bp) in self.rite_in.possibles():
                p = ap * bp # Probability of the match occurring at all
                (ra, rb) = evaluate_results(an, bn, p, self.round_num)
                self.out.add_prob(an, ra)
                self.out.add_prob(bn, rb)
        if verbose:
            print('matchupdate',self.left_in,'+',self.rite_in,'=',self.out)
        self.out.update()

    def __str__(self):
        return 'match'

tree = []
matches = []
gatekeeper = None

def reset():
    global gatekeeper
    global matches
    global tree
    tree = []
    matches = []
    gatekeeper = None

def build_one_round():
    active = tree[-1]
    nxt = []
    for i in range(0,len(active),2):
        matches.append( Match( active[i], active[i+ 1] ) )
        nxt.append( matches[-1].out )
    for (i,p) in enumerate(nxt):
        p.y = i + 1
    tree.append( nxt )

def load(probability_to_assume_from_an_unpolled_match):
    global gatekeeper
    global matches
    global tree
    global unpolled_prob
    reset()
    unpolled_prob = probability_to_assume_from_an_unpolled_match
    with open('participants') as f:
        participants = list(filter(lambda s: len(s) > 0, map(lambda l: l.strip(),f)))
    if len(participants) % 2 == 1:
        gatekeeper = Position(participants.pop())
        if verbose:
            print('Your gatekeeper is:',list(gatekeeper.who.keys())[0])
    if not math.log(len(participants), 2).is_integer():
        print('In order to form a proper single-elimination bracket, one needs a power-of-two number of participants.')
        exit(2)
    tree = [ list(map(lambda s: Position(s),participants)) ]
    while len(tree[-1]) > 1:
        build_one_round()
    if gatekeeper:
        gatekeeper.round_num = 'Gatekeeper match!'
        tree[-1][0].round_num = 'Gatekeeper match!'
        tree[-1].append(gatekeeper)
        build_one_round()
        tree[-1][0].round_num = 'Winner'
    for p in tree[0]:
        p.update()
    return tree

def unpolled_matches():
    global matchups
    only_unpolled = filter(lambda x:x[1]['Result'] == None, matchups.items())
    if verbose:
        only_unpolled = list(only_unpolled)
        print('Unpolled:',only_unpolled)
    answer = list(map(lambda x: (x[1]['Poccur'], x[1]), only_unpolled))
    answer.sort(reverse = True, key = lambda x:(x[0],x[1]['k']))
    return answer

from . import util
from enum import auto, Enum
from html.parser import HTMLParser
from os import path
from sys import argv

import json
import requests
import time

verbose = '--verbose' in argv

def set_verbose(v=True):
    verbose = v

def luat(n,ats):
    for at in ats:
        if n == at[0]:
            return at[1]
    return ''

class Sought(Enum):
    RESULTS = auto()
    BOT_NAME_ALL_CAPS = auto()
    SEASON_TAG = auto()
    SEASON_VALUE = auto()
    VS = auto()
    BOLD = auto()
    VS_OR_NEW_SEASON = auto()
    OUTCOME = auto()
    OLD_HEADERS = auto()
    OLD_HEADER_VAL = auto()
    OLD_SEASON = auto()
    OLD_SEASON_VAL = auto()
    OLD_SEASON_END_TD = auto()
    OLD_WL = auto()
    OLD_WL_VS = auto()
    END = auto()


def reset_info(season):
    return { 'season': season, 'cond': 'Unspecified' }


#TODO the older format is very much in place for a lot of the bots of past eras. It usually looks something like this:
#<table class="article-table" border="0" cellpadding="1" cellspacing="1" style="width: 500px;">
#<tbody><tr>
#<th scope="col"><b>Competition</b>
#</th>
#<th scope="col"><b>Wins</b>
#</th>
#<th scope="col"><b>Losses</b>
#</th></tr>
#<tr>
#<td>Long Beach 1999
#</td>
#<td>None
#</td>
#<td><a href="/wiki/FrenZy" title="FrenZy">frenZy</a>, <a href="/wiki/Punjar" title="Punjar">Punjar</a>
#</td></tr>
#<tr>
#<td>Season 1.0
#</td>
#<td><a href="/wiki/Nightmare" title="Nightmare">Nightmare</a>
#</td>
#<td><a href="/wiki/KillerHurtz" title="KillerHurtz">KillerHurtz</a>
#</td></tr>
#<tr>
#<td>Season 2.0
#<!-- snip ... -->
#<td><a href="/wiki/MechaVore" title="MechaVore">MechaVore</a>
#</td></tr></tbody></table>
class Parser(HTMLParser):
    last = Sought.END
    def __init__(self):
        super().__init__()
        self.me = ''
        self.seeking = Sought.RESULTS
        self.building = None
        self.results = []
        self.needle = ''
    def handle_starttag(self, tag, attrs):
        #if verbose:
            #print('<',tag,attrs,'>', self.seeking, self.building)
        if self.seeking == Sought.RESULTS:
            if luat('id', attrs) == 'Results':
                self.seeking = Sought.BOT_NAME_ALL_CAPS
            elif tag == 'span' and 'Wins' in luat('id',attrs) and 'Losses' in luat('id',attrs):
                if verbose:
                    print(attrs)
                self.seeking = Sought.OLD_HEADERS
                self.needle = 'Competition'
        elif (self.seeking == Sought.SEASON_TAG or self.seeking == Sought.VS_OR_NEW_SEASON) and tag == 'td' and luat('colspan',attrs) == '3' and '767676' in luat('style',attrs):
                self.seeking = Sought.SEASON_VALUE
        elif self.seeking == Sought.VS or self.seeking == Sought.VS_OR_NEW_SEASON:
            if tag == 'a' and luat('href',attrs).startswith('/wiki/'):
                vs = luat('title',attrs)
                if vs == self.me:
                    print('What is going on,',me,'did not fight itself.')
                    exit(1)
                self.building['vs'] = vs
                self.seeking = Sought.BOLD
        elif self.seeking == Sought.BOLD:
            if tag == 'b':
                self.seeking = Sought.OUTCOME
        elif self.seeking == Sought.OLD_HEADERS and tag == 'th' and luat('scope',attrs) == 'col':
            self.seeking = Sought.OLD_HEADER_VAL
        elif self.seeking == Sought.OLD_HEADER_VAL:
            #if verbose:
                #print('psyche')
            self.seeking = Sought.RESULTS
        elif self.seeking == Sought.OLD_SEASON and tag == 'td':
            self.seeking = Sought.OLD_SEASON_VAL
        elif self.seeking == Sought.OLD_WL and tag == 'td':
            self.seeking = Sought.OLD_WL_VS
        elif self.seeking == Sought.OLD_WL_VS and tag == 'a':
            self.building['vs'] = luat('title',attrs)
            #if verbose:
                #print(tag,attrs,self.building)

    def handle_endtag(self, tag):
        #if verbose:
            #print( self.seeking, self.building, '</',tag,'>' )
        if self.seeking == Sought.OLD_SEASON_END_TD:
            if tag == 'td':
                self.seeking = Sought.OLD_WL
        elif self.seeking == Sought.OLD_WL_VS:
            was_win = self.building['won']
            if tag == 'p' or tag == 'td':
                if 'vs' in self.building:
                    self.results.append(self.building)
                #elif verbose:
                    #print('Dropping result with no opponent:',json.dumps(self.building))
                self.building = reset_info(self.building['season'])
                if tag == 'p':
                    self.building['won'] = was_win
                else:
                    self.building['won'] = not was_win
            elif tag == 'tr':
                #if verbose:
                    #print('Moving to the next season.',self.building, self.seeking)
                self.seeking = Sought.OLD_SEASON
                self.building = None
                #if verbose:
                    #print('Moved to the next season.',self.building, self.seeking)
        elif self.seeking == Sought.OLD_WL:
            if tag == 'td':
                self.building['won'] = not self.building['won']
            elif tag == 'tr':
                #print('Moving to the next season with no results of that type.',self.building, self.seeking)
                self.seeking = Sought.OLD_SEASON
                self.building = None
        elif self.seeking != Sought.RESULTS and self.seeking != Sought.OLD_HEADERS and tag == 'table':
            if verbose:
                print('Saw a table end while seeking',self.seeking)
            if self.seeking == Sought.BOT_NAME_ALL_CAPS:
                if verbose:
                    print('Skipped a whole table, possibly due to bot alias.')
            else:
                self.seeking = Sought.END
        #if verbose:
            #print(  '</',tag,'>', self.seeking, self.building )

    def handle_data(self,data):
        if verbose and 'TBC' in data:
            print(self.building,'processing TBC data:',data)
        data = data.strip()
        if len(data) == 0:
            #if verbose:
                #print('Empty data while seeking',self.seeking)
            if self.seeking == Sought.OLD_HEADER_VAL and self.needle == 'Competition':
                self.needle = 'Wins'# Some of the older articles skip this header for whatever reason
                self.seeking = Sought.OLD_HEADERS
        elif self.seeking == Sought.SEASON_VALUE:
            self.building = reset_info(data)
            self.seeking = Sought.VS
        elif self.seeking == Sought.BOT_NAME_ALL_CAPS:
            if self.me.upper() in data:
                self.seeking = Sought.SEASON_TAG
                if verbose:
                    print('Found my name')
            else:
                if verbose:
                    print('Not my name: ',data)
        elif self.seeking == Sought.OUTCOME:
            if verbose:
                print(self.building,'Outcome to parse:',data)
            self.building['won'] = 'Won' in data
            self.building['lost'] = 'Lost' in data or 'Loss' in data
            cond = data
            if '(' in cond:
                cond = cond.split('(')[1]
            if ')' in cond:
                cond = cond.split(')')[0]
            if cond == 'Won' or cond == 'Loss':
                cond = 'Unspecified'
            self.building['cond'] = cond
            if verbose:
                print(self.building)
            self.results.append(self.building)
            self.building = reset_info( self.results[-1]['season'] )
            self.seeking = Sought.VS_OR_NEW_SEASON
        elif self.seeking == Sought.OLD_HEADER_VAL:
            if data != self.needle:
                self.seeking = Sought.RESULTS
            elif self.needle == 'Competition':
                self.seeking = Sought.OLD_HEADERS
                self.needle = 'Wins'
            elif self.needle == 'Wins':
                self.seeking = Sought.OLD_HEADERS
                self.needle = 'Losses'
            elif self.needle == 'Losses':
                self.seeking = Sought.OLD_SEASON
                self.needle = ''
        elif self.seeking == Sought.OLD_SEASON_VAL:
            self.building = reset_info( data )
            self.building['won'] = True
            self.seeking = Sought.OLD_SEASON_END_TD
        elif self.seeking == Sought.OLD_WL_VS and '(' in data:
            cond = data.split('(')[1].split(')')[0]
            if cond == 'Won' or cond == 'Loss':
                self.building['cond'] = 'Unspecified'
            else:
                self.building['cond'] = cond


def winloss_record(bot):
    if not bot:
        return []
    fn = util.escape_name(bot) + '.wins'
    if path.isfile(fn):#TODO what if it's very very old.
        with open(fn) as f:
            return json.load(f)
    time.sleep(1.2)
    if False and path.isfile('a.html'):
        with open('a.html') as f:
            html = f.read()
    else:
        html = requests.get( util.tourl(bot) ).content.decode()
        if verbose:
            with open('a.html','w') as f:
                f.write(html)
    parser = Parser()
    parser.me = bot
    parser.feed(html)
    sane = list(filter(lambda x:x['vs'] != bot,parser.results))
    with open(fn, 'w') as f:
        f.write(json.dumps(sane,indent=2))
    return sane

def either_or_both(w,l,k,x):
    if x > 0:
        return w[k]
    elif x < 0:
        return l[k]
    else:
        #print(w,l,k,x)
        return [ w[k], l[k] ]

def is_loss(x):
    if x['won']:
        return False
    if 'lost' in x:
        return x['lost']
    return True

def lifetime_winloss(bot):
    r = winloss_record(bot)
    w = sum(map(lambda x:x['won'], r))
    l = sum(map(is_loss, r))
    return (w,l)

def conquests(bot):
    r = winloss_record(bot)
    score = {}
    example = {}
    for m in r:
        k = m['vs']
        if m['won']:
            w = 1
        elif not 'lost' in m:
            w = -1
        elif m['lost']:
            w = -1
        else:
            w = 0
        if k in score:
            score[k] += w
        else:
            score[k] = w
        if m['won']:
            example[k] = m
    did_beat = lambda x:x[1] > 0
    and_how = lambda x: (x[0], example[x[0]])
    return dict(map(and_how,filter(did_beat, score.items())))

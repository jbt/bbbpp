
from datetime import datetime, timezone
from dateutil.parser import isoparse
from glob import glob
from os import path, system
from sys import exit

import json
import time

from . import bbcom, compare, misskey, opts, util

verbose = opts.args.verbose
private = opts.args.private 
dry     = opts.args.dry_run

START_TIME = datetime.now(timezone.utc)
START_PTIME = time.time()

POLL_HOURS = 77

class Poll:
    
    def __init__(self, key_or_filename, json_data = None):
        if key_or_filename.endswith('.json'):
            self.key = key_or_filename[0:-5]
        else:
            self.key = key_or_filename
        if json_data:
            self.jdata = json_data
            self.refreshed_at = time.time()
        else:
            try:
                with open(self.fn()) as f:
                    self.jdata = json.load(f)
            except Exception as e:
                print('Trouble loading ',self.fn(),':',e)
                exit(9)
            self.refreshed_at = path.getmtime(self.fn())
        if not 'pending' in self.jdata:
            self.jdata['pending'] = True
    
    def is_public(self):
        return 'visibility' in self.jdata and self.jdata['visibility'] == 'public'

    def get_key(self):
        return self.key
    
    def is_pending(self):
        return self.jdata['pending']
    
    def fn(self):
        return self.key + '.json'

    def i_voted(self):
        return True in map(lambda c: c['isVoted'], self.choices())
    
    def choices(self):
        return self.jdata['poll']['choices']

    def save(self):
        with open(self.fn(), 'w') as f:
            f.write(json.dumps(self.jdata,indent=2))
            
    def votes(self):
        rv = []
        for choice in enumerate(self.choices()):
            p = 0.95 - choice[0] * 0.15
            n = choice[1]['votes']
            rv += [p] * n
        return rv

    def mean(self):
        vs = self.votes()
        if vs:
            return sum(vs) / len(vs)
        else:
            return None

    def median(self):
        vs = self.votes() # Note these come out sorted in descending order
        if not vs:
            return None
        elif len(vs) % 2:
            return vs[len(vs)//2]
        else:
            return ( vs[len(vs)//2] + vs[len(vs)//2 - 1] ) / 2.0

    def freshness(self):
        return self.refreshed_at
    
    def chronological(self):
        if verbose:
            print( self.freshness(), self.jdata['createdAt'], self.key )
        return ( self.freshness(), self.jdata['createdAt'], self.key )
    
    def end_time(self):
        return isoparse(self.jdata['poll']['expiresAt'])

    def staleness(self):
        if START_PTIME > self.freshness():
            return START_PTIME - self.freshness()
        else:
            return 0
    
    def note_id(self):
        return self.jdata['id']
        
    def refresh(self):
        resp = misskey.request('notes/show', {"noteId": self.note_id()})
        if resp:
            self.jdata = resp.data
            self.jdata['pending'] = self.end_time() >= START_TIME
            self.save()
            if not self.i_voted():
                print('Go vote!', self.key, self.gui_url())
                system('xdg-open '+self.gui_url())
                exit(2)
            if self.is_pending():
                print(self.get_key(),' successfully refreshed.')
            else:
                print(self.get_key(),' first refresh after expiring. Unpin.')
                misskey.request('i/unpin',{"noteId": self.note_id()})
        else:
            print('Failed to refresh', get_key(), resp)
    
    def gui_url(self):
        return misskey.INSTANCE + 'notes/' + self.note_id()

_All = {}

def s():
    if not _All:
        for fn in glob('*.*.json'):
            if len(fn.split('.')) == 3:
                p = Poll(fn)
                if p.is_public():
                    _All[p.get_key()] = p
                else:
                    print('Ignoring your private poll',fn)
    return _All

def get(k):
    if k in s():
        return s()[k]
    else:
        return None

def freshen():
    ps = list(filter(Poll.is_pending, s().values()))
    #ps.sort(reverse=True, key=Poll.chronological)
    ps.sort(key=Poll.chronological)
    cache_age = 60
    for p in ps:
        if p.end_time() > START_TIME and p.staleness() < cache_age:
            print('not refreshing ',p.get_key())
            break
        cache_age += 4e3
        p.refresh()

def create(info, comment):
    a = info['a']
    b = info['b']
    (ai, au) = bbcom.profile_urls(a)
    if not au:
        au = util.tourl(a)
    (bi, bu) = bbcom.profile_urls(b)
    if not bu:
        bu = util.tourl(b)
    r = info['Round']
    ( ha, hb ) = compare.do_comparison( a, b, 1234 )
    swap_hist_order = ha and hb and (len(hb) < len(ha))
    ha = compare.path_desc_long( ha, True, au )
    hb = compare.path_desc_long( hb, True, bu )
    if verbose:
        print(f"ha={ha} hb={hb} swap={swap_hist_order}")
    if type(r) == int:
        r = 'Round ' + str(r)
    txt = f"{r} Prediction Poll: \n  [{a}]({util.tourl(a)})   vs.  [{b}]({util.tourl(b)}) \n\n"
    txt += f"  {comment} \n\n"
    if swap_hist_order:
        txt += f"Historical context:\n  {hb} \n  {ha} \n\n"
    elif ha and hb:
        txt += f"Historical context:\n  {ha} \n  {hb} \n\n"
    elif ha:
        txt += f"Historical context:\n  {ha}\n\n"
    elif hb:
        txt += f"Historical context:\n  {hb}\n\n"
    else:
        txt += 'Unprecedented.\n'
    txt +=  "#BattleBots #BBChamps"
    create_note = {
        'text': txt,
        "poll": {
                "choices": [
                    f"95% {a} wins", 
                    f"80% {a} wins", 
                    f"65% {a} wins", 
                    f"50/50 Even odds", 
                    f"65% {b} wins", 
                    f"80% {b} wins", 
                    f"95% {b} wins"
                ],
                "expiredAfter": POLL_HOURS * 3600 * 1000
            }
        }
    if private:
        create_note["visibility"] = "specified" # Default is public
        create_note["visibleUserIds"] = []
    j = create_note
    if dry:
        with open(info['k']+'.request', 'w') as f:
            f.write( json.dumps(j, indent=2) )
        return
    resp = misskey.request( 'notes/create', j )
    if resp:
        respj = resp.data['createdNote']
    else:
        print('Trouble creating a note? Bailing.',resp)
        exit(7)
    respj['pending'] = True
    resp_txt = json.dumps( respj, indent=2 )
    if verbose:
        print(resp_txt)
    with open(info['k']+'.json', 'w') as f:
        f.write( resp_txt )
    if not private:
        misskey.pin(respj['id'])

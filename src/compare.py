from . import util, wiki
from collections import deque
from sys import argv
verbose = '--verbose' in argv

beatable = {}

def declare(b,i):
    rv = f"beat {b}"
    if i['cond'] != 'Unspecified':
        rv += f' by {i["cond"]}'
    return rv + ' in "' + i['season'] + '"'

NOT_FOUND = 987654321

def degrees_to_beat( a, b, max_deg=99, to_here=set([]) ):
    global beatable
    if len(a) == 0 or a in to_here:
        return (NOT_FOUND,' did not beat '+b)
    to_here = to_here.union({ a })
    ar = wiki.conquests(a)
    if not a in beatable:
        beatable[a] = set(ar.keys())
    else:
        beatable[a].union(set(ar.keys()))
    desc = ''
    deg = max_deg
    print(a,':',b,'in',list(ar.keys()),(b in ar))
    if b in ar:
        s = declare(b,ar[b])
        print(a,s)
        return (1, s)
    elif max_deg > 0:
        for (c,i) in ar.items():
            if not c:
                continue
            if c == a:
                print('dont beat  yourself up',i)
                exit(1)
            if verbose:
                print('a={},b={},c={}'.format(a,b,c))
            (d,s) = degrees_to_beat( c, b, deg - 1, to_here )
            beatable[a] = beatable[a].union(beatable[c])
            if d < deg:
                s0 = declare( c, ar[c] )
                print(' ' * d, a, s0)
                desc = s0 + ' who ' + s
                deg = d
    if desc:
        return ( deg + 2, desc )
    else:
        return (NOT_FOUND,' did not beat '+b)

def can_beat(bot):
    l = list(beatable[bot])
    l.sort()
    return l


class Searcher:
    def __init__(self,start,end):
        self.a = start
        self.b = end
        self.wq = deque()
        self.lq = deque()
        self.wq.append( [ (start,{}) ] )
        self.lq.append( [ (end  ,{}) ] )
        self.answer = None
        self.winspath = {}
        self.losspath = {}
        self.winspath[self.a.lower()] = {}
        self.losspath[self.b.lower()] = {}
    def proc_one(self):
        if self.exhausted():
            return
        wh = self.winspath[self.a.lower()]
        lh = self.losspath[self.b.lower()]
        p = self.wq.popleft()
        bot = p[-1][0]
        if bot in wh:
            if len(wh[bot]) > len(p):
                wh[bot] = p
            else:
                return
        else:
            wh[bot] = p
        if verbose:
            lowerbots = list(self.winspath[self.a.lower()].keys())
            lowerbots.sort()
            print(self.a,'can beat',lowerbots)
            print(f"{self.a} processing '{bot}' while looking for '{self.b}'" )
        if bot.lower() == self.b.lower():
            if verbose:
                print(self.a,self.b,bot,self.answer,p)
            if self.answer:
                if len(p) < len(self.answer):
                    self.answer = p
            else:
                self.answer = p
            return
        cs = wiki.conquests(bot)
        for c in cs.items():
            #if verbose:
                #print(self.a,'Appending',p,'+',c)
            self.wq.append( p + [ c ] )
    def best(self):
        if self.answer:
            return len(self.answer)
        if self.exhausted():
            return NOT_FOUND
        return len(self.wq[0])
    def exhausted(self):
        return len(self.wq) == 0
    def solved(self):
        if self.answer == None:
            return False
        elif self.exhausted():
            print(self.a,'exhausted')
            return True
        else:
            #if verbose:
                #print(self.a,'solved?',self.answer,len(self.answer),'<',len(self.wq[0]),self.wq[0],'?',(len(self.answer) < len(self.wq[0])))
            return len(self.answer) < len(self.wq[0])
    def done(self):
        return self.solved() or self.exhausted()

format_with_links = False

def match_desc_short(t):
    bot = t[0]
    (w,l) = wiki.lifetime_winloss(bot)
    wl = "({:2d},{:2d})".format(w,l)
    if format_with_links:
        u = util.tourl(bot)
        result = f"[{bot}]({u})"
    else:
        result = bot
    if len(bot) < 12:
        result += ' ' * (12 - len(bot))
    return result + wl

def path_desc_short(a,linked=False):
    if not a:
        return ''
    global format_with_links
    format_with_links = linked
    d = ' > '.join(map(match_desc_short,a))
    return '{:3d} steps: '.format(len(a)-1) + d

def describe_match(tup):
    h = tup[1]
    cond = h['cond']
    if cond and cond != 'Unspecified':
        cond = ' by '+cond
    else:
        cond = ''
    vs = h['vs']
    if format_with_links:
        u = util.tourl(vs)
        vs = f"[{vs}]({u})"
    #if verbose:
        #print(f"describe_match({tup}), format_with_links={format_with_links}=>vs={vs}")
    season = h['season']
    return f"beat {vs}{cond} in \"{season}\""

def path_desc_long(answer,linked,first_uri):
    global format_with_links
    if verbose:
        print(f"path_desc_long({answer},{linked})")
    if not answer:
        return ''
    format_with_links = linked
    result = answer[0][0]
    if first_uri:
        result = f"[{result}]({first_uri})"
    result += ' ' + ' who '.join(map(describe_match,answer[1:]))
    return result

def do_comparison(a, b, max_steps=999):
    left = Searcher( a, b )
    rite = Searcher( b, a )
    for step in range(int(max_steps)):
        left.proc_one()
        rite.proc_one()
        if left.best() < rite.best():
            left.proc_one()
        if left.best() > rite.best():
            rite.proc_one()
        if not left.done():
            left.proc_one()
        if not rite.done():
            rite.proc_one()
        if left.done() and rite.done():
            break
    if verbose:
        print('Left:' ,left.answer)
        print('Right:',rite.answer)
    return ( left.answer, rite.answer )

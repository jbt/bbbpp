from sys import exit
import json
import requests
import time

#TODO: these should be configurable
USERNAME = 'john'
HOST = 'mk.nixnet.social'
INSTANCE = f"https://{HOST}/"
INSTANCE_API = INSTANCE + 'api/'

ME = { 'username': USERNAME, 'host': HOST }

with open('token') as f:
    token = f.read().strip()

class Response:
    def __init__(self, status_code, j):
        if status_code == 200:
            self.data = j
            self.ec = 0
            self.error = {}
        else:
            self.ec = status_code
            self.data = None
            self.error = j['error']
            print('API ERROR:',status_code,'\n',json.dumps(j,indent=2))

    def __bool__(self):
        return self.ec == 0

    def __str__(self):
        if self.ec:
            return f"{str(self.ec)} = {self.error}"


def request(endpoint, data):
    if not data:
        print('Expected json data to be provided')
        return None
    data["i"] = token
    time.sleep(1.1)
    resp = requests.post(INSTANCE_API + endpoint, json=data)
    return Response( resp.status_code, resp.json() )

def user_info(username=USERNAME , host=HOST):
    #Hitting .data directly, even though it will be None, because this is abortable error
    return request('users/show', { 'username': USERNAME, 'host': HOST }).data

def poll_sort_order(n):
    if n['poll']:
        vote_count = sum(map(lambda c:c['votes'], n['poll']['choices']))
    else:
        vote_count = 0
    return ( -vote_count, n['createdAt'] )

def pin(note_id):
    resp = request('i/pin', {'noteId': note_id} )
    if resp:
        print('Pinned:', note_id)
    elif resp.error['code'] == "PIN_LIMIT_EXCEEDED":
        #Too many pins!
        already_pinned = user_info()['pinnedNotes']
        if len(already_pinned) < 5:
            print('Was the failure not about having too many pins?')
            exit(8)
        already_pinned.sort(key = poll_sort_order)
        for p in already_pinned:
            if request('i/unpin', {'noteId': p['id']}):
                return pin(note_id)
            else:
                print('Trouble unpinning!!')
    else:
        print('Trouble pinning a note!', str(resp))
        exit(9)


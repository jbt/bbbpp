#!/usr/bin/env python3

from datetime import datetime
from requests import get
from sys import argv
from time import sleep 

import re

try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def bot_photo(bot_name,uri):
    fn = uri.split('/')[-1].lower()
    for w in bot_name.split(' '):
        if not w.lower() in fn:
            return False
    return 'bot' in fn

def profile_urls(bot_name):
    b = re.sub(r"[^a-z0-9\ ]", '', bot_name.lower()).replace(' ', '-')
    for year in range(datetime.now().year,2014,-1):
        try_url = f"https://battlebots.com/robot/{b}-{year}/"
        sleep(1.1)
        resp = get(try_url)
        if resp.status_code == 200:
            html = BeautifulSoup(resp.content,'html.parser')
            for img in html.find_all('img'):
                src = img.get('src')
                if bot_photo(bot_name, src):
                    return ( src, try_url )
            return ( '', try_url )


if '__main__' == __name__:
    for arg in argv[1:]:
        print(profile_urls(arg))

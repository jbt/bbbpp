import argparse

parser = argparse.ArgumentParser()
for (a,h) in [("--dry-run","Assume no new notes will be created and exit after reporting info"), 
              ("--private", "Any poll posted should not be visible to the public."),
              ("--unpolled-is-even", "For polls that have not yet been created, or have not received votes, assume the outcome is 50/50 even odds."),
              ('--verbose', 'Output more verbose/detailed logging.') ]:
    parser.add_argument(a, action='count', default=False, help=h)
    

args = parser.parse_args()

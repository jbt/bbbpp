import re
from urllib.parse import quote_plus

file_unsafe = r"[\s\*\?\.]"
url_unsafe = r"[\s\*]"

def escape_name(n):
    return re.sub(file_unsafe,'_',n)

def match_key(a,b):
    return escape_name(a) + '.' + escape_name(b)

def tourl(n):
    return 'https://battlebots.fandom.com/wiki/' + quote_plus( re.sub(url_unsafe,'_',n) )
